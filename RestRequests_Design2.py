from ast import Str
import datetime
from typing import List
import dateutil.parser as dp
import requests
from requests.models import Response
import requests_mock
import json

class AssetDateTimeConverter:
    def toDateTime(self, dateString : Str) -> datetime.datetime:
        if dateString.endswith("Z") or dateString.endswith(":00"):
            dateString = dateString.replace('Z', '')
            return dp.parse(dateString)
        else:
            raise Exception("Expected DateTime, but did not find Datestring.")

class Asset: 
    def __init__(self, att1 : Str, att2 : Str, dateTimeConverter : AssetDateTimeConverter) -> None:
        self.dateTimeConverter = dateTimeConverter
        self.att1 = att1
        self.att2 = self.dateTimeConverter.toDateTime(att2)
        
    @classmethod
    def fromJson(cls, data):
        return cls(**data)

    @classmethod
    def fromJsonString(cls, data):
        print(data)
        return cls(**json.loads(data))

    def toJson(self):
        return json.dumps(self.__dict__, ensure_ascii=False, indent=4)

    def toString(self):
        return str(self.toJson())
    
class AssetList:
    def __init__(self):
        self.assets : List[Asset] = []
        pass
    
    def addAsset(self, asset : Asset):
        self.assets.append(asset)
        
    def getAllAssets(self) -> List[Asset]:
        return self.assets


class Datasource:
    def getAllAssets(self) -> Str:
        pass
    
class AssetDatasource(Datasource):
    def getAllAssets(self) -> Str:
        return requests.get("http://localhost/api/assetList/").json()
    
class DataRepository:
    def __init__(self, datasource : Datasource, assetDateTimeConverter : AssetDateTimeConverter):
        self.datasource = datasource
        self.assetDateTimeConverter = assetDateTimeConverter
        self.assetList = AssetList()
        
    def getAllAssets(self) -> AssetList:
        for assetDict in json.loads(self.datasource.getAllAssets()):
            assetDict["dateTimeConverter"] = self.assetDateTimeConverter
            self.assetList.addAsset(Asset.fromJson(assetDict))
        return self.assetList
    
#How would we test something like this?

import unittest

class TestDatabase(unittest.TestCase):
    def test_getDeviceListComplete(self):
        myTestJson = r"""[{ "att1": "att1Test", "att2": "2022-03-30T14:43:59.561000Z" }, { "att1": "att2Test", "att2": "2022-03-30T14:43:59.561000Z" }]"""
        with requests_mock.Mocker() as m:
            m.register_uri('GET', 'http://localhost/api/assetList/', json=myTestJson)
            assetList = DataRepository(AssetDatasource(), AssetDateTimeConverter()).getAllAssets()
            cmp_dt = datetime.datetime(year=2022,month=3, day=30, hour=14, minute=43, second=59, microsecond=561000)
            self.assertEqual(len(assetList.getAllAssets()), 2)
            self.assertEqual(assetList.getAllAssets()[0].att1, "att1Test")
            self.assertEqual(assetList.getAllAssets()[0].att2, cmp_dt)
            self.assertEqual(assetList.getAllAssets()[1].att1, "att2Test")
            self.assertEqual(assetList.getAllAssets()[1].att2, cmp_dt)
            
    def test_AssetDateTimeConverter(self):
        dt = "2022-03-30T14:43:59.561000Z"
        adtc = AssetDateTimeConverter()
        converted = adtc.toDateTime(dt)
        self.assertTrue(isinstance(converted, datetime.datetime))
        
        
class MyDatasource(Datasource):
    def __init__(self,str) -> None:
        self.str = str
    def getAllAssets(self) -> Str:
        return self.str

class TestDatabaseNoMock(unittest.TestCase): # well sort of no mock, but it's way easier.
    def test_getDeviceListCompleteNoMock(self):
        myTestJson = r"""[{ "att1": "att1Test", "att2": "2022-03-30T14:43:59.561000Z" }, { "att1": "att2Test", "att2": "2022-03-30T14:43:59.561000Z" }]"""
        cmp_dt = datetime.datetime(year=2022,month=3, day=30, hour=14, minute=43, second=59, microsecond=561000)
        assetList = DataRepository(MyDatasource(myTestJson), AssetDateTimeConverter()).getAllAssets()
        self.assertEqual(len(assetList.getAllAssets()), 2)
        self.assertEqual(assetList.getAllAssets()[0].att1, "att1Test")
        self.assertEqual(assetList.getAllAssets()[0].att2, cmp_dt)
        self.assertEqual(assetList.getAllAssets()[1].att1, "att2Test")
        self.assertEqual(assetList.getAllAssets()[1].att2, cmp_dt)
        
unittest.main()

#Why is this design better?

# 1. It let's you test 100 % of the code.
# 2. Most of the code can be tested without mocking
# 3. Seperation of concern, 
#    3.1 the Database Object is put into an object
#        that is easily readable, changable, ..
#        you get an object that you don't have to know
#        or lookup inside a database, it's outlined and
#        it autocompletes, which means: you don't have to
#        know any of the other code to work with this
#    3.2 the AssetList can be used to implement filters, ...
#    3.3 the datasource can be exchanged at will (see testcases)
#    3.4 ...

#Why is this better testable?
# 1. You won't need a mock for 90% of the code to be testable
# 2. The parsing of the object makes testing easier (asserting, 
#    if things are where they need to be)
# 3. The steps are testable, which means debugging them becomes
#    faster and easier.

#How to write testable code?
# 1. Start with the test. And test everything, 100% coverage.
# 2. Seperate concerns into different classes
# 3. Dependencies that could be exchanged, should be injected

#What makes a testcase a good testcase?
# 1. Defined input and output
# 2. Independent
# 3. Always yields the same result, no matter if outside 
#    references stay the same.