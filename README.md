# README #

![UML Diagram](./classes.png)

# Why is this design better?

1. It let's you test 100 % of the code.
2. Most of the code can be tested without mocking
3. Seperation of concern, 
    -  the Database Object is put into an object
       that is easily readable, changable, ..
       you get an object that you don't have to know
       or lookup inside a database, it's outlined and
       it autocompletes, which means: you don't have to
       know any of the other code to work with this
    -  the AssetList can be used to implement filters, ...
    -  the datasource can be exchanged at will (see testcases)
    -  ...

# Why is this better testable?

1. You won't need a mock for 90% of the code to be testable
2. The parsing of the object makes testing easier (asserting, 
   if things are where they need to be)
3. The steps are testable, which means debugging them becomes
   faster and easier.

# How to write testable code?

1. Start with the test. And test everything, 100% coverage.
2. Seperate concerns into different classes
3. Dependencies that could be exchanged, should be injected


# What makes a testcase a good testcase?

1. Defined input and output
2. Independent
3. Always yields the same result, no matter if outside 
   references stay the same.