import datetime
import dateutil.parser as dp
import requests
from requests.models import Response
import requests_mock

class Database:
    def __init__(self):
        pass

    #private
    def __toDateTime(self, dateString):
        if dateString.endswith("Z") or dateString.endswith(":00"):
            dateString = dateString.replace('Z', '')
            return dp.parse(dateString)
        else:
            return dateString
    #private
    def __convertAllDatesRecursive(self, dictionary):
        if (type(dictionary) is dict):
            for key in dictionary:
                if (type(dictionary[key]) is dict):
                    dictionary[key] = self.__convertAllDatesRecursive(dictionary[key])
                elif (type(dictionary[key]) is list):
                    self.__convertAllDatesRecursive(dictionary[key])
                else:
                    try:
                        dictionary[key] = self.__toDateTime(dictionary[key])
                    except:
                        pass
            return dictionary
        elif (type(dictionary) is list):
            for i in range(len(dictionary)):
                dictionary[i] = self.__convertAllDatesRecursive(dictionary[i])
            return dictionary

    def getDeviceListComplete(self):
        response = requests.get("http://localhost/api/assetList/")
        print(response.json())
        returnJson = response.json()
        return self.__convertAllDatesRecursive(returnJson)
    
#How would we test something like this?

import unittest

class TestDatabase(unittest.TestCase):
    def test_getDeviceListComplete(self):
        myTestJson = r"""[{ "att1": "att1Test", "att2": "2022-03-30T14:43:59.561000Z" }, { "att1": "att2Test", "att2": "2022-03-30T14:43:59.561000Z" }]"""
        with requests_mock.Mocker() as m:
            m.register_uri('GET', 'http://localhost/api/assetList/', json=myTestJson)
            response = requests.get("http://localhost/api/assetList/")
            returnJson = Database().getDeviceListComplete()
            self.assertEquals(returnJson, myTestJson)
            
            #I conclude = we have a problem, the request is working just fine, 
            #but we are not able to write a unit test case that tests
            #__convertAllDatesRecursive without breaking the design.
            #How could this be done different to make testing easier,
            #Mocking (which is not a nice solutions anyhow) unnecessary,
            #and make the design better?
            
            #Honest to god, Idk why this is not working,
            #but: it is from a live project ;)
            
unittest.main()